'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ngRoute',
//   'ngCookies',
	'ngMessages',
    'ngMaterial',
    'myApp.view1',
    'myApp.register',
    'myApp.version'
])
	.config(['$routeProvider', '$locationProvider', '$httpProvider', '$mdThemingProvider', '$mdIconProvider', '$provide',
		function ($routeProvider, $locationProvider, $httpProvider, $mdThemingProvider, $mdIconProvider, $provide) {
			$locationProvider.html5Mode(true);

			$routeProvider
				.when('/', {
					templateUrl: 'views/home/homeView.html',
					controller: 'HomeController',
					access: { allowAnonymous: true }
				})
				.when('/feed', {
					templateUrl: 'views/feed/feedView.html',
					controller: 'FeedController',
					access: { allowAnonymous: true }
				})
				.when('/feed/:userName', {
					templateUrl: 'views/feed/feedView.html',
					controller: 'FeedController',
					access: { allowAnonymous: true }
				})
				.when('/register', {
					templateUrl: 'registerView/register.html',
					controller: 'registerCtrl',
					access: { allowAnonymous: true }
				})

				.otherwise({
					redirectTo: '/'
				});

			//THEMES
			var customBlueMap = $mdThemingProvider.extendPalette('light-blue', {
				'contrastDefaultColor': 'light',
				'contrastDarkColors': ['50'],
				'50': 'ffffff'
			});
			$mdThemingProvider.definePalette('customBlue', customBlueMap);
			$mdThemingProvider.theme('default')
				.primaryPalette('customBlue', {
					'default': '800',
					'hue-1': '50'
				})
				.accentPalette('pink');
			$mdThemingProvider.theme('input', 'default')
				.primaryPalette('grey')
			//.config(function($mdIconProvider) {
			//ICONS
			$mdIconProvider
				.iconSet('communication', 'img/icons/sets/communication-icons.svg', 24)
				.iconSet('core', 'img/icons/sets/core-icons.svg', 24);
		}])
	.run(['$rootScope', '$location', '$http', 'CustomerService', function ($rootScope, $location, $http, CustomerService) {
		$rootScope.$on('$routeChangeStart', function (event, next, current) {
			if (!$rootScope.isLoggedIn) {
				if (angular.isUndefined(next.access)) {
					event.preventDefault();
					$location.path('/');
				}
			}
		});
	}]).run(['CustomerService', '$rootScope', function (CustomerService, $rootScope) {
		CustomerService.isLoggedIn().then(function successCallback(response) {
			if (response.data === true) $rootScope.isLoggedIn = true;
			else $rootScope.isLoggedIn = false;
		}, function errorCallback(response) {
			//alert("Epic Failure, please try again");
		});
		$rootScope.$on('postUpdated', function () {
			console.log('got to root')
			$rootScope.$broadcast('updatePosts');
		})
	}]);

