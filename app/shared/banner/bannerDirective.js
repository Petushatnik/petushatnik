angular.module('myApp').directive('appBanner',bannerDirective);
function bannerDirective () {
    return {
        restrict: 'E',
	    replace: true,
	    transclude: true,
	    scope:{
    		bannerImage: "=bannerImage",
            bannerImageStyle: "=bannerImageStyle",
            bannerBackground: "=",
            bannerSize:"&",
            bannerHeight:"&",
            backgroundSize:"&" ,
            keepSize:"&"
	    },
        templateUrl: 'shared/banner/bannerView.html',
        link:function(scope, element, attrs){
            scope.isImage = typeof scope.bannerImage !== "undefined"
            var source = scope.bannerImage || scope.bannerBackground,
                bannerHeight = "auto",
                background = "100% 100%"

            var img = new Image();
            img.src = source;
            img.onload = function() {
                if (scope.isImage){
                    element.css({
                        height: this.height+'px',
                        width: '100%'
                    }).find("img").css({
                        position:'absolute',
                        zIndex: '0'
                    });
                    element.find('div.content').css({
                        zIndex: '1'
                    });
                } else {
                    if(attrs.keepSize){
                        background = this.width+'px '+this.height+'px';
                        bannerHeight = this.height+'px';
                    } else if(attrs.bannerHeight && attrs.backgroundSize){
                        background = attrs.backgroundSize;
                        bannerHeight = attrs.bannerHeight;
                    } else if(attrs.backgroundSize){
                        background = attrs.backgroundSize;
                        bannerHeight = this.height+'px'; 
                    } else {
                       background = attrs.bannerSize?attrs.bannerSize:'100% '+this.height+'px'; 
                       bannerHeight = this.height+'px'; 
                    }                
                    element.css({
                        backgroundImage:'url('+this.src+')',
                        backgroundSize: background,
                        height: bannerHeight
                    });
                }
            }
        }
    };
}