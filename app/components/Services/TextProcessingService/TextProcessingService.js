angular.module('myApp').factory('TextProcessingService', function() {
	var factory = {};
	
	factory.parseTags = function(data) {
		var regEx = /#?\w+#/g
		return data.match(regEx);
	}
	return factory;	
})