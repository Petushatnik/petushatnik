angular.module('myApp').factory('CustomerService', ['$http', function ($http) {
	var factory = {};

	factory.loginCustomer = function (data) {

		var obj = {
			"email": data.email,
			"password": data.password,
			"rememberMe": data.remember
		};

		var headers = {
            'Access-Control-Allow-Credentials': 'true',
        }
		var request = $http({
			method: 'POST',
			url: 'http://kurjatnik.azurewebsites.net/api/User/Login',
			headers: headers,//{'Access-Control-Allow-Credentials': 'true'},
			withCredentials: true,
			data: obj
		});

		return request;
	}

	factory.registerCustomer = function (data) {
		var obj = {
            "email": data.email,
            "profileName": data.name,
            "password": data.password,
            "confirmPassword": data.password2
		};
		var request = $http({
			method: 'POST',
			url: 'http://kurjatnik.azurewebsites.net/api/User/Register',
			data: obj
		});

		return request;
	}

	factory.logoutCustomer = function () {
		var request = $http({
			method: 'POST',
			url: "http://kurjatnik.azurewebsites.net/api/User/Logout",
			withCredentials: true
		});

		return request;
	}

	factory.isLoggedIn = function () {
		var request = $http({
			method: 'GET',
			url: "http://kurjatnik.azurewebsites.net/api/User/IsLoggedIn",
			withCredentials: true
		});
		return request;
	}

	return factory;
}]);
