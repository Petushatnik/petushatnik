angular.module('myApp').factory('PostService', ['$http', function($http) {
	var factory = {};

	factory.postMessage = function(data) {
		data.Title = "we need no title";
		var obj =
			{
				"title": "we need no title",
				"content": data.body

			};
		var headers = {
            'Access-Control-Allow-Credentials': 'true',
        }
		var request = $http({
			method: 'POST',
			url: 'http://kurjatnik.azurewebsites.net/api/Posts',
			headers: headers,//{'Access-Control-Allow-Credentials': 'true'},
			withCredentials: true,
			data: data
		});
		return request;
	}

	
	factory.getFeed = function(ammount, skip, userName) {
		var url = angular.isUndefined(userName) ? 'http://kurjatnik.azurewebsites.net/api/Posts/GetWallFeed':
				'http://kurjatnik.azurewebsites.net/api/Posts/getUserFeed/'  + userName;
				
		var obj = {
			"take": ammount,
			"skip": skip

		}
		var headers = {
			'Access-Control-Allow-Credentials': 'true',
		}

		var request = $http({
			method: 'POST',
			url: url,
			headers: headers,//{'Access-Control-Allow-Credentials': 'true'},
			withCredentials: true,
			data: obj
		})
		return request;		
	}

	return factory;
}])
