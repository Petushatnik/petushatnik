/**
 * Created by Ruslan on 3/31/2016.
 */
angular.module('myApp').directive('appSidenav',sidenav);
function sidenav () {
    return {
        restrict: 'EA',
        replace: true,
        controller: 'MainController',
        templateUrl: 'components/directives/sidenavDirective/sidenavView.html'
    };
}