/**
 * Created by Ruslan on 3/31/2016.
 */
angular.module('myApp').directive('appHeader',['CustomerService', function(CustomerService) {
    return {
        restrict: 'E',
        replace: true,
        controller: 'MainController',
        templateUrl: 'components/directives/headerDirective/headerView.html',
         link: function (scope, elem, attrs) {

         }
    };
}]);