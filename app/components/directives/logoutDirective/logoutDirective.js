angular.module('myApp').directive('logoutDirective', ['CustomerService', '$location','$rootScope', function (CustomerService, $location, $rootScope) {
	return {
		restrict: 'E',
		templateUrl: 'components/directives/logoutDirective/logoutDirective.html',
		scope: {
			sideMenu:'='
		},
		link: function (scope, elem, attrs) {
			scope.send = function(){
				CustomerService.logoutCustomer().then(function successCallback(response) {
                        $location.path( "/" );
						$rootScope.isLoggedIn = false;
                    }, function errorCallback(response) {						
                          alert("Epic Failure, please try again");
                    });
			}
		}
	}
}]);