angular.module('myApp').directive('registerDirective', ['CustomerService','$location','$mdDialog', function(CustomerService, $location, $mdDialog) {
    return {
        restrict: 'E',
        templateUrl: 'components/directives/registerDirective/registerDirective.html',
        controller:'MainController',
        scope: {},
        link: function(scope, elem, attrs) {
            
            scope.user = { normal:true};
            
            scope.change = function() {
                if (scope.form.password2.$pristine) return;
                if (scope.form.password.$viewValue !== scope.form.password2.$viewValue) {
                    scope.form.password2.$setValidity('Confirm', false);
                } else scope.form.password2.$setValidity('Confirm', true);
            };

            scope.passwordPattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/
            scope.emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

            scope.send = function(data) {
                CustomerService.registerCustomer(data)
                    .then(function successCallback(response) {
                        console.log('success');
                        console.log(response);
                        $location.path('feed');

                    }, function errorCallback(response) {
                        var regEx = /User.*taken/
                        if (regEx.test(response.data)) alert("User already exists!");
                         else alert("Epic Failure, please try again");
                    });
            };

			scope.rapeCommonSense = function(ev){
			$mdDialog.show({
                    controller: function(scope,$mdDialog){scope.hide = function() {$mdDialog.hide();}},
                    templateUrl: 'shared/dialogs/TandC/termsandconditions.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true
                });

		}



            //       scope.login = function () {
            //         console.log("logging in")
            //         var obj = {
            //           "email": "string11@gmail.com",
            //           "password": "Qwe123",
            //           "rememberMe": false
            //         };
            // 
            //         var headerz = {
            //           'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
            //           'Content-Type': 'application/json',
            //           'Accept': 'application/json',
            //           'Access-Control-Allow-Credentials': 'true'
            //         };
            // 
            //         var headers = {
            //           'Authorization': 'Basic d2VudHdvcnRobWFuOkNoYW5nZV9tZQ==',
            //           'Access-Control-Allow-Credentials': 'true',
            //           'Accept': 'application/json;odata=verbose'
            //         }
            //         $http({
            //           method: 'POST',
            //           url: 'http://kurjatnik.azurewebsites.net/api/User/Login',
            //           headers: headers,//{'Access-Control-Allow-Credentials': 'true'},
            //           withCredentials: true,
            //           data: obj
            //         }).then(function successCallback(response) {
            // 
            //           console.log('login success');
            //           console.log(response);
            // 
            // 
            //         }, function errorCallback(response) {
            //           console.log('login fail');
            //           console.log(response);
            // 
            //         });
            //       }
            // 
            //       scope.isLoggedIn = function () {
            // 
            //         $http({
            //           method: 'GET',
            //           url: "http://kurjatnik.azurewebsites.net/api/Test/IsLoggedIn",
            // 
            //           withCredentials: true
            //         }).then(function successCallback(response) {
            //           console.log('login check success');
            //           console.log(response);
            // 
            //         }, function errorCallback(response) {
            //           console.log('login check fail');
            //           console.log(response);
            //         });
            //         console.log(" login check");
            //       }
            // 
            //       scope.logout = function () {
            //         $http({
            //           method: 'POST',
            //           url: "http://kurjatnik.azurewebsites.net/api/User/Logout",
            //           withCredentials: true
            //         }).then(function successCallback(response) {
            //           console.log('logout  success');
            //           console.log(response);
            // 
            //         }, function errorCallback(response) {
            //           console.log('logout  fail');
            //           console.log(response);
            //         });
            //       }
        }
    }
}]);