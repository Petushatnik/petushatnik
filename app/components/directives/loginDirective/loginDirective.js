angular.module('myApp').directive('loginDirective', ['CustomerService', '$location', '$rootScope', function (CustomerService, $location, $rootScope) {
  return {
    restrict: 'E',
    templateUrl: 'components/directives/loginDirective/loginDirective.html',
    scope: {},
    link: function (scope, elem, attrs) {
      scope.user = { remember: true };
      scope.showError = false;

      scope.send = function (data) {
        if (!data.email || !data.password) return;
        scope.showError = false;
        CustomerService.loginCustomer(data).then(function successCallback(response) {
          console.log('success');
          $rootScope.isLoggedIn = true;
          $location.path("feed");
        }, function errorCallback(response) {
          scope.showError = true;
          // alert("Epic Failure, please try again");
        });
        console.log(data)
      }

    }
  }
}]);