angular.module('myApp').directive('createPost', ['PostService','TextProcessingService', '$mdDialog',function( PostService, TextProcessingService, $mdDialog) {
	return {
        restrict: 'E',
        templateUrl: 'components/directives/createPostDirective/createPostView.html',
		scope: {},
		link: function(scope, elem, attrs) {
			scope.message = {
				body:""
			};	

			scope.send = function(data) {
				scope.genericError = false;
				var tags = TextProcessingService.parseTags(data.body);
				var obj = {
					Content: data.body,
					Title: ""
					// ,tags: tags
				}

				PostService.postMessage(obj).then(function successCallback(response) {
					console.log('success');
					console.log(response);
					scope.$emit('postUpdated')
					scope.message.body = "";
					$mdDialog.hide();

				}, function errorCallback(response) {
					scope.genericError = false;
					// alert("Epic Failure, please try again");
				});
			}
		}
    }
}]);
